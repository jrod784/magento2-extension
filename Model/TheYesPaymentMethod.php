<?php

namespace Theyes\Theyespayment\Model;
 
/**
 * TheYes payment method model
 */
class TheYesPaymentMethod extends \Magento\Payment\Model\Method\AbstractMethod
{
 
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'theyespayment';
 
    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;
}